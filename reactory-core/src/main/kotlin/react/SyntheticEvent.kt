@file:JsModule("react")
@file:JsNonModule
package react

import org.w3c.dom.events.Event
import org.w3c.dom.events.EventTarget

// https://reactjs.org/docs/events.html

external interface SyntheticEvent<T : EventTarget, E : Event> {
    var bubbles: Boolean
    var cancelable: Boolean
    var currentTarget: T
    var defaultPrevented: Boolean
    var eventPhase: Long
    var isTrusted: Boolean
    var nativeEvent: E
    fun preventDefault()
    fun isDefaultPrevented(): Boolean
    fun stopPropagation()
    fun isPropagationStopped(): Boolean
    var target: EventTarget
    var timeStamp: Long
    var type: String
}
