package react

fun <T> RBuilder.childFunc(func: (T) -> Any) {
    childList.add(func)
}
