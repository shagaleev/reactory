package react

fun useEffect(effect: () -> Unit, dependencies: RDependenciesList? = null) =
    useEffect(dependencies, effect)

fun useEffectWithCleanup(effect: () -> RCleanup, dependencies: RDependenciesList? = null) =
    useEffectWithCleanup(dependencies, effect)

fun useLayoutEffect(effect: () -> Unit, dependencies: RDependenciesList? = null) =
    useLayoutEffect(dependencies, effect)

fun useLayoutEffectWithCleanup(effect: () -> RCleanup, dependencies: RDependenciesList? = null) =
    useLayoutEffectWithCleanup(dependencies, effect)

fun <T : Function<*>> useCallback(callback: T, dependencies: RDependenciesList? = null): T =
    useCallback(callback, dependencies?.toTypedArray() ?: emptyArray())

fun <T : Function<*>> useCallback(callback: T): T =
    useCallback(callback, emptyArray())

fun <T> useMemo(callback: () -> T, dependencies: RDependenciesList? = null): T =
    useMemo(callback, dependencies?.toTypedArray() ?: emptyArray())

fun <T> useMemo(callback: () -> T): T =
    useMemo(callback, emptyArray())

fun <T> useRef(): RMutableRef<T> =
    rawUseRef()

