@file:JsModule("react")
@file:JsNonModule
package react

@JsName("useRef")
external fun <T> rawUseRef(): RMutableRef<T>
