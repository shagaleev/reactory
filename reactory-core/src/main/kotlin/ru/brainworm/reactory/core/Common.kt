package ru.brainworm.reactory.core

import ru.brainworm.reactory.core.reactory.ReactoryImpl
import ru.brainworm.reactory.core.reactory.feature.Lang

fun <Ctx : Context> createReactory(vararg features: ReactoryFeature): Reactory<Ctx> =
    ReactoryImpl(*features)

interface Context

interface Action

typealias Dispatch = (action: Action) -> Unit

typealias Reducer<Ctx> = (action: Action, context: Ctx) -> Ctx

typealias Suspend = suspend (action: Action) -> Unit

data class Store<Ctx : Context>(
    var context: Ctx,
    var dispatch: Dispatch
)

interface ReactoryFeature

interface Reactory<Ctx : Context> {
    /**
     * Применить экшон
     */
    fun dispatch(action: Action)

    /**
     * Добавить редьюсер в список зарегистрированных редьюсеров для дальнейшей обработки вызовов метода [dispatch]
     */
    fun registerReducer(reducer: Reducer<Ctx>)

    /**
     * Добавить суспенд в список зарегистрированных суспендов для дальнейшей обработки вызовов метода [dispatch]
     */
    fun registerSuspension(suspension: Suspend)

    /**
     * Хук для получения хранилища (контекста и диспатчера)
     * Использовать ТОЛЬКО внутри функционального компонента!
     */
    fun useStore(): Store<Ctx>

    /**
     * Функция для получения хранилища (контекста и диспатчера)
     * Использовать везде, КРОМЕ функциональных компонентов (в суспендах к примеру)
     */
    fun getStore(): Store<Ctx>

    /**
     * Хук для получения языкового пакета
     * Использовать ТОЛЬКО внутри функционального компонента!
     */
    fun useLang(): Lang

    /**
     * Функция для получения языкового пакета
     * Использовать везде, КРОМЕ функциональных компонентов (в суспендах к примеру)
     */
    fun getLang(): Lang
}

data class ApplyActionWithoutBuildIns(val real: Action) : Action

typealias GetStore<Ctx> = () -> Store<Ctx>

typealias DispatchMiddleware<Ctx> = (action: Action, getStore: GetStore<Ctx>, next: Dispatch) -> Unit
