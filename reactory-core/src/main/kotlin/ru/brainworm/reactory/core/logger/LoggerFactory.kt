package ru.brainworm.reactory.core.logger

import ru.brainworm.reactory.core.logger.formatter.LoggerFormatter
import kotlin.reflect.KClass

object LoggerFactory {

    const val NAME_ALL = ""

    fun getLogger(name: KClass<*>): Logger =
        getLogger(name.simpleName ?: NAME_ALL)

    fun getLogger(name: String = NAME_ALL): Logger =
        object : Logger {
            override fun getName(): String = name
            override fun isTraceEnabled(): Boolean = isLogEnabled(LogLevel.trace())
            override fun trace(message: String, e: Throwable?): Unit = log(LogLevel.trace(), message, e)
            override fun isDebugEnabled(): Boolean = isLogEnabled(LogLevel.debug())
            override fun debug(message: String, e: Throwable?): Unit = log(LogLevel.debug(), message, e)
            override fun isInfoEnabled(): Boolean = isLogEnabled(LogLevel.info())
            override fun info(message: String, e: Throwable?): Unit = log(LogLevel.info(), message, e)
            override fun isWarnEnabled(): Boolean = isLogEnabled(LogLevel.warn())
            override fun warn(message: String, e: Throwable?): Unit = log(LogLevel.warn(), message, e)
            override fun isErrorEnabled(): Boolean = isLogEnabled(LogLevel.error())
            override fun error(message: String, e: Throwable?): Unit = log(LogLevel.error(), message, e)
            override fun isLogEnabled(level: Int): Boolean = isLogEnabled(name, level)
            override fun log(level: Int, message: String, e: Throwable?): Unit = log(name, level, message, e)
        }

    fun addConfiguration(vararg configurations: LoggerConfiguration) {
        addConfiguration(configurations.toList())
    }

    fun addConfiguration(configurations: Iterable<LoggerConfiguration>) {
        LoggerFactory.configurations.addAll(configurations)
    }

    fun setConfiguration(vararg configurations: LoggerConfiguration) {
        setConfiguration(configurations.toList())
    }

    fun setConfiguration(configurations: Iterable<LoggerConfiguration>) {
        LoggerFactory.configurations.clear()
        LoggerFactory.configurations.addAll(configurations)
    }

    fun defaultConfiguration(): LoggerConfiguration =
        LoggerConfiguration()

    private fun log(name: String, level: Int, message: String, e: Throwable?): Unit =
        configurations
            .filterByName(name)
            .filterByLevel(level)
            .forEach { configuration ->
                val levelName = LogLevel.getName(level)
                val msg = LoggerFormatter.format(configuration.pattern, name, levelName, message)
                configuration.appenders.forEach { it.onLog(level, msg, e) }
            }

    private fun isLogEnabled(name: String, level: Int): Boolean =
        configurations
            .filterByName(name)
            .filterByLevel(level)
            .any()

    private fun List<LoggerConfiguration>.filterByName(name: String): List<LoggerConfiguration> =
        filter { it.names.contains(name) }
       .ifEmpty { filter { it.names.contains(NAME_ALL) } }

    private fun List<LoggerConfiguration>.filterByLevel(level: Int): List<LoggerConfiguration> =
        filterNot { it.level >= LogLevel.none() }
       .filter { it.level <= level }

    private val configurations = mutableListOf(defaultConfiguration())
}
