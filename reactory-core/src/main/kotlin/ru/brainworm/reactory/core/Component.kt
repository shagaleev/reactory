package ru.brainworm.reactory.core

import react.FunctionalComponent
import react.RProps

@Deprecated("use functionalComponent<RProps>(name)")
fun <P : RProps> FunctionalComponent<P>.withDisplayName(
    name: String
) = also { it.asDynamic().displayName = name }

fun <P : RProps> FunctionalComponent<P>.withDisplayNameIfNotSet(
    name: String
) = also {
    val component = it.asDynamic()
    if (component.displayName == undefined || component.displayName == "") {
        component.displayName = name
    }
}
