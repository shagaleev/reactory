package ru.brainworm.reactory.core.descriptor

data class FieldDescriptor<T>(
    val type: Type<T>,
    val nullable: TypeNull
)
