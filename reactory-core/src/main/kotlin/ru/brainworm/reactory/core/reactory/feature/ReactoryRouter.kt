package ru.brainworm.reactory.core.reactory.feature

import ru.brainworm.reactory.core.Action
import ru.brainworm.reactory.core.ReactoryFeature
import ru.brainworm.reactory.core.descriptor.FieldDescriptor
import kotlin.reflect.KClass

/**
 * Фича роутинга.
 *
 * @param mode [Режим работы роутинга][RouterMode].
 * @param removeInvalidRoutes Если **true**, то неверные роутинги будут автоматически удалены.
 *                            Если **false** - оставлены, при этом будет послан [NotFoundRouterAction].
 * @param routes Мапа всех роутингов в текущем приложении.
 *
 * @see RouterMode
 * @see RouterAction
 * @see BackRouterAction
 * @see NotFoundRouterAction
 */
data class RouterFeature(
    internal val mode: RouterMode,
    internal val removeInvalidRoutes: Boolean,
    internal val routes: Map<String, KClass<out RouterAction>>
) : ReactoryFeature

/**
 * Режим работы роутинга.
 *
 * [HASH] - Роутинг работает с **window.location.hash**
 *
 * [PATHNAME] - Роутинг работает с **window.location.pathname**
 */
enum class RouterMode {
    HASH,
    PATHNAME,
    ;
}

/**
 * Экшон для роутинга.
 *
 * Работает как обычный экшон, позволяет использовать его в качестве роутинга.
 *
 * Обязательные условия:
 * - Реализация интерфейса [RouterAction]
 * - Основной конструктор должен принимать мапу [Map<String, Any?>]
 * - Должны быть определены поля:
 *     - [rPrimary] - Первичность экшона. Если **true**, экшон всегда будет первый в стеке элементов истории.
 *     - [rFields] - Список полей для сериализации в адресную строку. Определяет названия полей, типы и
 *     возможности быть нуллом. **Осторожно!** Значения будут записаны в текущий класс ровно так, как они описаны.
 *     Любые не описанные дополнительные поля должны иметь возможность быть нуллом или иметь значения по умолчанию.
 *     Любые расхождения в типах или нарушения данного правила приведут к критическим ошибкам в рантайме.
 *
 * Пример экшона:
 *
 *     class ExampleAction(map: MutableMap<String, Any?>) : RouterAction {
 *         var id: Long by map
 *         var name: String? by map
 *
 *         override val rFields = mapOf(
 *             "id" to FieldDescriptor(TypeLong(), NullSafe),
 *             "name" to FieldDescriptor(TypeString(), Nullable)
 *         )
 *         override val rPrimary = false
 *
 *         constructor(id: Long, name: String?) : this(mutableMapOf()) {
 *             this.id = id
 *             this.name = name
 *         }
 *     }
 *
 * Настройка роутера:
 *
 *     createReactory(
 *         ...,
 *         RouterFeature(
 *             mode = RouterMode.HASH,
 *             removeInvalidRoutes = false,
 *             routes = mapOf(
 *                 "foo" to FooAction::class,
 *                 "bar" to BarAction::class,
 *                 "example" to ExampleAction::class,
 *                 ...
 *             )
 *         )
 *     )
 *
 * Адресная строка:
 *
 * `http://localhost:8080/#/foo/bar/example:id=4:name=John/bar`
 */
interface RouterAction : Action {
    val rFields: Map<String, FieldDescriptor<*>>
    val rPrimary: Boolean
}

/**
 * Специальный экшон для сдвига назад по стеку текущего роутинга. Если в стеке только один роутинг, тогда
 * ничего не происходит.
 */
class BackRouterAction : Action

/**
 * Экшон, посылаемый автоматически, если текущий роутинг является невалидным.
 */
data class NotFoundRouterAction(
    val path: String
) : Action
