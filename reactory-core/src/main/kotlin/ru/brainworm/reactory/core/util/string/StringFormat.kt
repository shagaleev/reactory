package ru.brainworm.reactory.core.util.string

typealias StringFormatPattern = String

/**
 * Форматирование строки по шаблону
 *
 * Пример:
 * ```kotlin
 * val template = "Hello {}, how {}"
 * val string = template.format("there", "are you")
 * assertTrue(string == "Hello there, how are you")
 * ```
 */
fun StringFormatPattern.format(vararg params: Any): String {
    var result = this
    params.forEach { param ->
        result = result.replaceFirst("{}", param.asString())
    }
    return result
}

/**
 * Получение подстроки из строки по шаблону и индексу
 *
 * Индекс 1-based
 *
 * Пример:
 * ```kotlin
 * val template = "Hello {}, how {}"
 * val string = "Hello there, how are you"
 * assertTrue("there" == template.formatFetchAtIndex(string, 1))
 * assertTrue("are you" == template.formatFetchAtIndex(string, 2))
 * ```
 */
fun StringFormatPattern.formatFetchAtIndex(value: String, index: Int): String? {
    val delimiter = "(\\{\\})".toRegex()
    val regex = Regex.escape(this)
        .replace("\\{", "{")
        .replace("\\}", "}")
        .split(delimiter)
        .reduceIndexed { idx, result, element ->
            val entry = if (idx == index) "(.+)" else ".+"
            result + entry + element
        }
        .toRegex()
    return regex.matchEntire(value)?.groups?.let { groups ->
        when (groups.size > 1) {
            true -> groups[1]?.value
            false -> null
        }
    }
}

private fun Any.asString(): String =
    when (this) {
        is String -> this
        else -> this.toString()
    }
