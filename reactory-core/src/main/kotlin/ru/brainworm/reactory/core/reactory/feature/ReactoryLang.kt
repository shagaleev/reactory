package ru.brainworm.reactory.core.reactory.feature

import kotlinx.browser.window
import ru.brainworm.reactory.core.Action
import ru.brainworm.reactory.core.ReactoryFeature
import ru.brainworm.reactory.core.js.getCurrentRoot

interface Lang {
    operator fun invoke(key: String, vararg parameters: Any): String
    val hash: Int
}

/**
 * Фича языковых пакетов
 */
data class LangFeature(
    internal val loadPath: String = window.location.getCurrentRoot() + "locales/{lng}",
    internal val version: String? = null
) : ReactoryFeature

/**
 * Экшон загрузки языкового пакета
 */
data class LoadLangAction(
    val locale: String,
    val actionsOnDone: () -> List<Action> = { listOf(LoadLangDoneAction(locale)) },
    val actionsOnError: (Exception) -> List<Action> = { listOf(LoadLangErrorAction(it)) }
) : Action

/**
 * Экшон (по умолчанию) успешной загрузки языкового пакета
 */
data class LoadLangDoneAction(
    val locale: String
) : Action

/**
 * Экшон (по умолчанию) неуспешной загрузки языкового пакета
 */
data class LoadLangErrorAction(
    val exception: Exception
) : Action
