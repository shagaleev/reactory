package ru.brainworm.reactory.core.http

import kotlinx.coroutines.await
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.json.Json
import org.w3c.fetch.Response
import ru.brainworm.reactory.core.util.Either
import ru.brainworm.reactory.core.util.right

interface HttpResponse<out T> {
    fun accept(): String
    suspend fun deserialize(response: Response): Either<HttpException, T>
}

open class EmptyHttpResponse : HttpResponse<Response> {
    override fun accept() = ""
    override suspend fun deserialize(response: Response): Either<HttpException, Response> = response.right()
}

open class PlainHttpResponse : HttpResponse<String> {
    override fun accept() = "text/plain"
    override suspend fun deserialize(response: Response): Either<HttpException, String> = response.text().await().right()
}

open class JsonHttpResponse<out T>(
    private val deserializer: DeserializationStrategy<T>,
    private val json: Json = Json
) : HttpResponse<T> {
    override fun accept(): String = "application/json"
    override suspend fun deserialize(response: Response): Either<HttpException, T> =
        Either.catch(
            { HttpParseException("Failed to parse response", it) },
            { json.decodeFromString(deserializer, response.text().await()) }
        )
}
