package ru.brainworm.reactory.core.reactory.feature

import react.FunctionalComponent
import react.RProps
import react.RSetState
import ru.brainworm.reactory.core.*
import ru.brainworm.reactory.core.util.Option

/**
 * Фича базового функционала рекатори
 */
data class CoreFeature<Ctx : Context>(
    internal val rootComponent: FunctionalComponent<RProps>,
    internal val rootContainerId: String,
    internal val context: Ctx,
    internal val reducers: List<Reducer<Ctx>>,
    internal val suspensions: List<Suspend>
) : ReactoryFeature

data class ReactoryContextModel<Ctx : Context>(
    var storeContext: Store<Ctx>,
    var langContext: Option<Lang>
)

internal interface ReactoryInternal<Ctx : Context> : Reactory<Ctx> {
    fun registerBuiltInReducer(reducer: Reducer<Ctx>)
    fun registerBuiltInSuspension(suspension: Suspend)
    var lang: Option<Lang>
    var updateLang: RSetState<Option<Lang>>?
}
