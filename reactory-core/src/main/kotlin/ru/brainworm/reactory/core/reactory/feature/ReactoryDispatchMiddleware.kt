package ru.brainworm.reactory.core.reactory.feature

import ru.brainworm.reactory.core.Context
import ru.brainworm.reactory.core.DispatchMiddleware
import ru.brainworm.reactory.core.ReactoryFeature

data class DispatchMiddlewareFeature<Ctx : Context>(
    val middlewares: List<DispatchMiddleware<Ctx>>
) : ReactoryFeature
