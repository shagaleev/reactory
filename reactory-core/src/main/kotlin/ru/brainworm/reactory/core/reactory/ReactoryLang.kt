package ru.brainworm.reactory.core.reactory

import kotlinx.coroutines.await
import kotlinx.serialization.json.*
import org.w3c.fetch.Response
import ru.brainworm.reactory.core.Action
import ru.brainworm.reactory.core.Context
import ru.brainworm.reactory.core.http.*
import ru.brainworm.reactory.core.reactory.feature.Lang
import ru.brainworm.reactory.core.reactory.feature.LoadLangAction
import ru.brainworm.reactory.core.reactory.feature.ReactoryInternal
import ru.brainworm.reactory.core.util.Either
import ru.brainworm.reactory.core.util.map
import ru.brainworm.reactory.core.util.some
import ru.brainworm.reactory.core.util.string.format

internal class ReactoryLang<Ctx : Context>(
    private val reactory: ReactoryInternal<Ctx>,
    private val langLoadPath: String,
    private val langLoadVersion: String?
) {

    private var json: JsonObject? = null
    private val loadPathExtension = "json"
    private val langReplaceKey = "{lng}"

    init {
        reactory.registerBuiltInSuspension(::langSuspensions)
    }

    private suspend fun langSuspensions(action: Action): Unit = when (action) {
        is LoadLangAction -> {
            val locale = action.locale
            val onDone = action.actionsOnDone
            val onError = action.actionsOnError
            val (_, dispatch) = reactory.getStore()
            load(langLoadPath, langLoadVersion, locale)
                .fold(
                    { onError(it).forEach(dispatch) },
                    {
                        reactory.lang = object : Lang {
                            override fun invoke(key: String, vararg parameters: Any): String = i18n(key, *parameters)
                            override val hash: Int = (json?.toString() ?: "").hashCode()
                        }.some()
                        reactory.updateLang?.invoke(reactory.lang)
                        onDone().forEach(dispatch)
                    }
                )
        }
        else -> Unit
    }

    private suspend fun load(url: String, version: String?, locale: String): Either<Exception, Unit> =
        load(buildUrl(url, locale, version))
            .map { it.jsonObject }
            .map { json = it; it }
            .map { Unit }

    private fun buildUrl(url: String, lang: String, version: String?): String =
        url.replace(langReplaceKey, lang.toLowerCase())
            .plus(".").plus(loadPathExtension)
            .plus(version?.let { "?v=$it" } ?: "")

    private suspend fun load(url: String): Either<Exception, JsonElement> =
        Http.get(
            url = url,
            request = EmptyHttpRequest(),
            response = object : HttpResponse<JsonElement> {
                override fun accept(): String = "application/json"
                override suspend fun deserialize(response: Response): Either<HttpException, JsonElement> =
                    Either.catch(
                        { HttpParseException("Failed to parse lang", it) },
                        { Json.parseToJsonElement(response.text().await()) }
                    )
            }
        )

    private fun i18n(key: String, vararg parameters: Any): String {
        if (json == null) {
            return ""
        }
        val element = json?.get(key)
        if (element == null || element is JsonNull || element !is JsonPrimitive) {
            throw IllegalArgumentException("Lang for key '$key' not found, null or not primitive: $element")
        }
        val value = element.contentOrNull ?: ""
        if (parameters.isEmpty()) {
            return value
        }
        return value.format(*parameters)
    }
}
