package ru.brainworm.reactory.core.log

import ru.brainworm.reactory.core.logger.LoggerFactory

internal val log = LoggerFactory.getLogger("reactory")
