package ru.brainworm.reactory.core.util

data class Id<out A>(private val value: A) {

    inline fun <NA> flatMap(f: (A) -> Id<NA>): Id<NA> =
        f(extract())

    inline fun <NA> map(f: (A) -> NA): Id<NA> =
        Id(f(extract()))

    fun extract(): A = value

    companion object {
        fun <A> just(a: A) = Id(a)
    }

    override fun equals(other: Any?): Boolean = when (other) {
        is Id<*> -> other.value == value
        else -> other == value
    }

    override fun hashCode(): Int {
        return value?.hashCode() ?: 0
    }
}

fun <A> A.idJust(): Id<A> = Id.just(this)
