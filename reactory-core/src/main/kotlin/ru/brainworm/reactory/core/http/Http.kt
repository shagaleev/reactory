package ru.brainworm.reactory.core.http

import kotlinx.browser.window
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.await
import org.w3c.dom.AbortController
import org.w3c.fetch.RequestInit
import org.w3c.fetch.Response
import ru.brainworm.reactory.core.util.Either
import ru.brainworm.reactory.core.util.flatMap
import ru.brainworm.reactory.core.util.left
import ru.brainworm.reactory.core.util.right
import kotlin.js.json

object Http {

    suspend fun <T, S> get(
        url: String,
        request: HttpRequest<T>,
        response: HttpResponse<S>,
        headers: Map<String, String> = hashMapOf()
    ): Either<HttpException, S> =
        exchange("GET", url, request, response, headers)

    suspend fun <T, S> post(
        url: String,
        request: HttpRequest<T>,
        response: HttpResponse<S>,
        headers: Map<String, String> = hashMapOf()
    ): Either<HttpException, S> =
        exchange("POST", url, request, response, headers)

    suspend fun <T, S> exchange(
        method: String,
        url: String,
        request: HttpRequest<T>,
        response: HttpResponse<S>,
        headers: Map<String, String> = hashMapOf(),
        requestInit: RequestInit.() -> Unit = {}
    ): Either<HttpException, S> =
        request.serialize()
            .flatMap { fetch(method, url, buildHeaders(headers, request, response), it, requestInit) }
            .flatMap { checkResponseSucceed(it) }
            .flatMap { response.deserialize(it) }

    private suspend fun fetch(
        method: String,
        url: String,
        headers: Map<String, String>,
        body: Any?,
        requestInit: RequestInit.() -> Unit
    ): Either<HttpException, Response> =
        Either.catchSuspend(
            { HttpException(it) },
            {
                val abortController = AbortController()
                try {
                    val init: RequestInit =
                        object : RequestInit {
                            override var method: String? = method
                            override var headers: dynamic = json(*headers.toList().toTypedArray())
                            override var body: dynamic = body
                        }
                        .apply(requestInit)
                        .apply {
                            this.asDynamic().signal = abortController.signal
                        }
                    val promise = window.fetch(url, init)
                    promise.await()
                } catch (e: CancellationException) {
                    abortController.abort()
                    throw e
                }
            }
        )

    private fun <T, S> buildHeaders(
        headers: Map<String, String>,
        request: HttpRequest<T>,
        response: HttpResponse<S>
    ): Map<String, String> = headers
        .let {
            val accept = response.accept()
            if (accept.isNotEmpty() && !it.containsKey("Accept")) {
                it.plus("Accept" to accept)
            } else it
        }
        .let {
            val contentType = request.contentType()
            if (contentType.isNotEmpty() && !it.containsKey("Content-Type")) {
                it.plus("Content-Type" to contentType)
            } else it
        }

    private fun checkResponseSucceed(response: Response) : Either<HttpException, Response> =
        if (response.ok) {
            response.right()
        } else {
            HttpStatusException(response.status, response.statusText, response).left()
        }
}
