package ru.brainworm.reactory.core.logger

interface Logger {
    fun getName(): String
    fun isLogEnabled(level: Int): Boolean
    fun log(level: Int, message: String, e: Throwable? = null)
    fun isTraceEnabled(): Boolean
    fun trace(message: String, e: Throwable? = null)
    fun isDebugEnabled(): Boolean
    fun debug(message: String, e: Throwable? = null)
    fun isInfoEnabled(): Boolean
    fun info(message: String, e: Throwable? = null)
    fun isWarnEnabled(): Boolean
    fun warn(message: String, e: Throwable? = null)
    fun isErrorEnabled(): Boolean
    fun error(message: String, e: Throwable? = null)
}
