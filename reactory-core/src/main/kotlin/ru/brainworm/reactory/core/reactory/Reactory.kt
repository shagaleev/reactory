package ru.brainworm.reactory.core.reactory

import kotlinx.browser.document
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import react.*
import react.dom.render
import ru.brainworm.reactory.core.*
import ru.brainworm.reactory.core.js.queueMacrotask
import ru.brainworm.reactory.core.reactory.feature.*
import ru.brainworm.reactory.core.util.Option
import ru.brainworm.reactory.core.util.firstOrNone
import ru.brainworm.reactory.core.util.none

class ReactoryImpl<Ctx : Context>(
    vararg features: ReactoryFeature
) : Reactory<Ctx>, ReactoryInternal<Ctx> {

    private lateinit var ReactoryContext: RContext<ReactoryContextModel<Ctx>>
    private lateinit var store: Store<Ctx>
    override var lang: Option<Lang> = none()
    override var updateLang: RSetState<Option<Lang>>? = null
    private var updateStore: RSetState<Store<Ctx>>? = null
    private var reducerList: List<Reducer<Ctx>> = emptyList()
    private var reducerBuildInList: List<Reducer<Ctx>> = emptyList()
    private var suspensionList: List<Suspend> = emptyList()
    private var suspensionBuildInList: List<Suspend> = emptyList()
    private var dispatchMiddlewares: List<DispatchMiddleware<Ctx>> = emptyList()
    private lateinit var rootFuncComponent: FunctionalComponent<RProps>

    private val baseComponent = functionalComponent<RProps>("baseComponent") {
        val (ctx, updateCtx) = useState(ReactoryContextModel(store, lang))
        updateStore = useCallback({ store: Store<Ctx> ->
            updateCtx(ctx.copy(storeContext = store))
        }, listOf(ctx, updateCtx))
        updateLang = useCallback({ lang: Option<Lang> ->
            updateCtx(ctx.copy(langContext = lang))
        }, listOf(ctx, updateCtx))
        StrictMode {
            ReactoryContext.Provider(ctx) {
                child(rootFuncComponent.withDisplayNameIfNotSet("rootComponent"))
            }
        }
    }

    init {
        features.filterIsInstance<LangFeature>()
            .firstOrNone()
            .map { initLang(it) }
        features.filterIsInstance<RouterFeature>()
            .firstOrNone()
            .map { initRoute(it) }
        features.filterIsInstance<DispatchMiddlewareFeature<Ctx>>()
            .firstOrNone()
            .map { initDispatchMiddlewares(it) }
        features.filterIsInstance<CoreFeature<Ctx>>()
            .firstOrNone()
            .fold(
                { throw IllegalArgumentException("Required feature (${CoreFeature::class.simpleName}) not found") },
                { initCore(it) }
            )
    }

    private fun initCore(core: CoreFeature<Ctx>) {
        reducerList = core.reducers
        suspensionList = core.suspensions
        store = Store(
            context = core.context,
            dispatch = ::dispatch
        )
        ReactoryContext = createContext()
        ReactoryContext.asDynamic().displayName = "ReactoryContext"
        rootFuncComponent = core.rootComponent
        val rootElement = document.getElementById(core.rootContainerId)
            ?: throw IllegalStateException("Container with id#${core.rootContainerId} not found")
        queueMacrotask {
            render(rootElement) {
                child(baseComponent)
            }
        }
    }

    private fun initLang(feature: LangFeature) {
        ReactoryLang(
            reactory = this,
            langLoadPath = feature.loadPath,
            langLoadVersion = feature.version
        )
    }

    private fun initRoute(feature: RouterFeature) {
        ReactoryRoute(
            reactory = this,
            mode = feature.mode,
            removeInvalidRoutes = feature.removeInvalidRoutes,
            routes = feature.routes
        )
    }

    private fun initDispatchMiddlewares(feature: DispatchMiddlewareFeature<Ctx>) {
        dispatchMiddlewares = feature.middlewares
    }


    override fun useStore(): Store<Ctx> =
        useContext(ReactoryContext).storeContext

    override fun getStore(): Store<Ctx> =
        store.copy()

    override fun useLang(): Lang =
        useContext(ReactoryContext).langContext.orElseGet<Lang> { throw IllegalStateException("Feature (LangFeature) not found") }

    override fun getLang(): Lang =
        lang.orElseGet<Lang> { throw IllegalStateException("Feature (LangFeature) not found") }


    override fun registerReducer(reducer: Reducer<Ctx>) {
        reducerList += reducer
    }

    override fun registerBuiltInReducer(reducer: Reducer<Ctx>) {
        reducerBuildInList += reducer
    }

    override fun registerSuspension(suspension: Suspend) {
        suspensionList += suspension
    }

    override fun registerBuiltInSuspension(suspension: Suspend) {
        suspensionBuildInList += suspension
    }


    override fun dispatch(action: Action) {
        withDispatchMiddlewares(action, dispatchMiddlewares.iterator()) { fAction ->
            when (fAction) {
                is ApplyActionWithoutBuildIns -> {
                    invokeReducers(reducerList, fAction.real, store.context).also { store.context = it }
                    invokeSuspensions(suspensionList, fAction.real)
                }
                else -> {
                    invokeReducers(reducerBuildInList, fAction, store.context).also { store.context = it }
                    invokeReducers(reducerList, fAction, store.context).also { store.context = it }
                    invokeSuspensions(suspensionBuildInList, fAction)
                    invokeSuspensions(suspensionList, fAction)
                }
            }
            updateStore?.invoke(store)
        }
    }

    private fun withDispatchMiddlewares(action: Action, middlewares: Iterator<DispatchMiddleware<Ctx>>, dispatch: Dispatch) {
        when {
            middlewares.hasNext() -> {
                middlewares.next()(action, ::getStore) { mAction ->
                    withDispatchMiddlewares(mAction, middlewares, dispatch)
                }
            }
            else -> dispatch(action)
        }
    }

    private fun invokeReducers(reducerList: List<Reducer<Ctx>>, action: Action, context: Ctx): Ctx =
        context.run {
            var ctx = this
            reducerList.forEach { reducer ->
                ctx = reducer(action, ctx)
            }
            ctx
        }

    private fun invokeSuspensions(suspensionList: List<Suspend>, action: Action): Unit =
        suspensionList.forEach { suspension ->
            GlobalScope.launch { suspension(action) }
        }
}
