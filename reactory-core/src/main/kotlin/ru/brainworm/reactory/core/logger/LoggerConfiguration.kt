package ru.brainworm.reactory.core.logger

import ru.brainworm.reactory.core.logger.LoggerFactory.NAME_ALL
import ru.brainworm.reactory.core.logger.appender.ConsoleLogAppender
import ru.brainworm.reactory.core.logger.appender.LogAppender
import ru.brainworm.reactory.core.logger.formatter.LoggerFormatter.DEFAULT_PATTERN

data class LoggerConfiguration(
    val pattern: String = DEFAULT_PATTERN,
    val level: Int = LogLevel.all(),
    val names: List<String> = listOf(NAME_ALL),
    val appenders: List<LogAppender> = listOf(ConsoleLogAppender())
)
