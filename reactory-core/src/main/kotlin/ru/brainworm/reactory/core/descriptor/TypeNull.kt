package ru.brainworm.reactory.core.descriptor

sealed class TypeNull

object Nullable : TypeNull()
object NullSafe : TypeNull()

fun <T> FieldDescriptor<T>.nullable(): FieldDescriptor<T> = copy(nullable = Nullable)
fun <T> FieldDescriptor<T>.nullsafe(): FieldDescriptor<T> = copy(nullable = NullSafe)
