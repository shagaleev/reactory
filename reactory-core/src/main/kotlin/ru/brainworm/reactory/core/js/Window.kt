package ru.brainworm.reactory.core.js

import org.w3c.dom.Node
import org.w3c.dom.Window

fun Node?.getWindow(): Window =
    this?.ownerDocument?.defaultView
        ?: kotlinx.browser.window
