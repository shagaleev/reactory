package ru.brainworm.reactory.core.js

import kotlinx.browser.window

internal fun <T : Any> JsClass<T>.newInstance(): T {
    @Suppress("UNUSED_VARIABLE")
    val ctor = this.asDynamic()
    return js("new ctor()") as T
}

internal fun <T> String.toNumber(): T {
    @Suppress("UNUSED_VARIABLE")
    val me = this
    return js("+me") as T
}

internal fun String.decodeURI(): String {
    @Suppress("UNUSED_VARIABLE")
    val me = this
    return js("decodeURI(me)") as String
}

internal fun queueMacrotask(task: () -> Unit): Unit =
    window.setTimeout(task, 0).let { Unit }
