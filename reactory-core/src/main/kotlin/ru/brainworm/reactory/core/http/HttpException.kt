package ru.brainworm.reactory.core.http

import org.w3c.fetch.Response

open class HttpException(
    message: String?,
    cause: Throwable?
) : Exception(message, cause) {
    constructor() : this(null, null)
    constructor(message: String?) : this(message, null)
    constructor(cause: Throwable?) : this(cause?.toString(), cause)
}

open class HttpStatusException(
    val status: Short,
    val statusText: String,
    val response: Response
) : HttpException()

open class HttpParseException(
    message: String?,
    cause: Throwable?
) : HttpException(message, cause) {
    constructor() : this(null, null)
    constructor(message: String?) : this(message, null)
    constructor(cause: Throwable?) : this(cause?.toString(), cause)
}
