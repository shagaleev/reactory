package ru.brainworm.reactory.core.logger

object LogLevel {
    fun all(): Int = 0
    fun trace(): Int = 20
    fun debug(): Int = 40
    fun info(): Int = 60
    fun warn(): Int = 80
    fun error(): Int = 100
    fun none(): Int = Int.MAX_VALUE

    fun getName(level: Int): String =
        customNames.getOrElse(level) {
            when (level) {
                all() -> "ALL"
                trace() -> "TRACE"
                debug() -> "DEBUG"
                info() -> "INFO"
                warn() -> "WARN"
                error() -> "ERROR"
                else -> "level-$level"
            }
        }

    fun addCustomName(level: Int, name: String) =
        customNames.put(level, name)

    private val customNames = mutableMapOf<Int, String>()
}
