package ru.brainworm.reactory.core.js

import org.w3c.dom.Location

fun Location.getCurrentRoot(): String =
    origin + pathname
