package ru.brainworm.reactory.core.http

import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.json.Json
import ru.brainworm.reactory.core.util.Either
import ru.brainworm.reactory.core.util.right

interface HttpRequest<out T> {
    fun contentType(): String
    fun serialize(): Either<HttpException, T?>
}

open class EmptyHttpRequest : HttpRequest<Any> {
    override fun contentType(): String = ""
    override fun serialize(): Either<HttpException, String?> = null.right()
}

open class PlainHttpRequest(
    private val body: String
) : HttpRequest<String> {
    override fun contentType(): String = "text/plain"
    override fun serialize(): Either<HttpException, String?> = body.right()
}

open class JsonHttpRequest<in T>(
    private val body: T,
    private val serializer: SerializationStrategy<T>,
    private val json: Json = Json
) : HttpRequest<String> {
    override fun contentType(): String = "application/json"
    override fun serialize(): Either<HttpException, String?> =
        Either.catch(
            { HttpParseException("Failed to serialize request", it) },
            { json.encodeToString(serializer, body) }
        )
}
