package ru.brainworm.reactory.core.logger.formatter

import kotlin.js.Date

object LoggerFormatter {

    const val DATETIME = "%dt"
    const val TIME = "%t"
    const val LEVEL = "%level"
    const val LOGGER = "%logger"
    const val MESSAGE = "%msg"

    const val DEFAULT_PATTERN = "[$TIME] $LEVEL [$LOGGER] - $MESSAGE"

    fun format(pattern: String, logger: String, level: String, message: String) =
        pattern
            .replace(DATETIME, Date().asDateTimeString())
            .replace(TIME, Date().asTimeString())
            .replace(LEVEL, level)
            .replace(LOGGER, logger.ifEmpty { "<root>" })
            .replace(MESSAGE, message)

    private fun Date.asDateTimeString(): String =
        StringBuilder()
            .append(getFullYear())
            .append(".")
            .append(getMonth().leadingZeros(2))
            .append(".")
            .append(getDate().leadingZeros(2))
            .append(" ")
            .append(getHours().leadingZeros(2))
            .append(":")
            .append(getMinutes().leadingZeros(2))
            .append(":")
            .append(getSeconds().leadingZeros(2))
            .append(".")
            .append(getMilliseconds())
            .toString()

    private fun Date.asTimeString(): String =
        StringBuilder()
            .append(getHours().leadingZeros(2))
            .append(":")
            .append(getMinutes().leadingZeros(2))
            .append(":")
            .append(getSeconds().leadingZeros(2))
            .append(".")
            .append(getMilliseconds())
            .toString()

    private fun Number.leadingZeros(length: Int): String {
        var value = toString()
        while (value.length < length) value = "0$value"
        return value
    }
}
