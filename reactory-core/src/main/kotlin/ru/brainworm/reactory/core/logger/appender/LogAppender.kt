package ru.brainworm.reactory.core.logger.appender

interface LogAppender {
    fun onLog(level: Int, message: String, e: Throwable? = null)
}
