package ru.brainworm.reactory.core.reactory

import kotlinx.browser.window
import ru.brainworm.reactory.core.Action
import ru.brainworm.reactory.core.ApplyActionWithoutBuildIns
import ru.brainworm.reactory.core.Context
import ru.brainworm.reactory.core.descriptor.FieldDescriptor
import ru.brainworm.reactory.core.descriptor.Nullable
import ru.brainworm.reactory.core.js.decodeURI
import ru.brainworm.reactory.core.js.queueMacrotask
import ru.brainworm.reactory.core.reactory.feature.*
import ru.brainworm.reactory.core.reactory.feature.ReactoryInternal
import ru.brainworm.reactory.core.util.*
import kotlin.reflect.KClass

internal class ReactoryRoute<Ctx : Context>(
    private val reactory: ReactoryInternal<Ctx>,
    private val mode: RouterMode,
    private val removeInvalidRoutes: Boolean,
    routes: Map<String, KClass<out RouterAction>>
) {

    private val path2action: Map<String, KClass<out RouterAction>>
    private val action2path: Map<KClass<out RouterAction>, String>
    private var routesStack = listOf<RouteItem>()

    init {
        path2action = routes.map { (path, action) -> path.normalizeAsPath() to action }.toMap()
        action2path = path2action.map { (path, action) -> action to path }.toMap()
        reactory.registerBuiltInSuspension(::routerSuspensions)
        initListenerForBrowserNavigation()
        forceHandleBrowserNavigation()
    }

    private suspend fun routerSuspensions(action: Action): Unit = when (action) {
        is BackRouterAction -> {
            if (routesStack.size >= 2) {
                val lastRoute = routesStack.last()
                routesStack = (routesStack - lastRoute).removeAllBeforePrimaryAction()
                routesStack.applyRoutesToBrowser()
                routesStack.fireLastAction()
            }
            Unit
        }
        is RouterAction -> {
            RouteItem.makeFromAction(action).map { route ->
                routesStack = (routesStack + route).removeAllBeforePrimaryAction()
                routesStack.applyRoutesToBrowser()
            }
            Unit
        }
        else -> Unit
    }

    private fun RouteItem.asString(): String =
        when (this) {
            is RouteItemInvalid -> route
            is RouteItemValid -> param2value
                .map { (name, value) ->
                    val descriptor = param2descriptor[name]
                    val serialized = descriptor?.type?.asString(value)
                    if (serialized.isNullOrEmpty()) {
                        return@map null
                    }
                    name to serialized
                }
                .filterNotNull()
                .joinToString(":") { (name, value) -> "$name=$value" }
                .let { parameters ->
                    path + if (parameters.isNotEmpty()) ":$parameters" else ""
                }
        }

    private fun RouteItem.Companion.makeFromRoute(route: String): RouteItem {
        val path = fetchPath(route)
        val context = makeActionParamDescriptors(path)
            .flatMap { param2descriptor ->
                val param2value = fetchParamValues(route, param2descriptor)
                val param2nullableValue = fillMissingValuesWithNulls(param2value, param2descriptor)
                makeActionForPath(path, param2nullableValue).map { action ->
                    RouteItemValid(
                        path = path,
                        param2value = param2value,
                        param2descriptor = param2descriptor,
                        action = action
                    )
                }
            }
        return context.fold(
            { RouteItemInvalid(route) },
            { it }
        )
    }

    private fun RouteItem.Companion.makeFromAction(action: RouterAction): Option<RouteItem> =
        action2path[action::class].toOption()
            .map { path ->
                RouteItemValid(
                    path = path,
                    param2value = action.getAccessibleFieldNamesWithValues(),
                    param2descriptor = action.getAccessibleFieldDescriptors(),
                    action = action
                )
            }

    private fun initListenerForBrowserNavigation(): Unit =
        window.addEventListener("popstate", { handleNavigationFromBrowser(getRoutesFromBrowser()) })

    private fun forceHandleBrowserNavigation(): Unit =
        queueMacrotask { handleNavigationFromBrowser(getRoutesFromBrowser()) }

    private fun handleNavigationFromBrowser(routes: List<RouteItem>): Unit {
        routesStack = routes
            .run { if (removeInvalidRoutes) retainOnlyValidRouteItems() else this }
            .removeAllBeforePrimaryAction()
            .apply { fireLastAction() }
        if (routes.size != routesStack.size) {
            routesStack.applyRoutesToBrowser()
        }
    }

    private fun Collection<RouteItem>.fireLastAction(): Unit {
        lastOrNone().fold(
            { reactory.dispatch(NotFoundRouterAction("")) },
            { route -> when (route) {
                is RouteItemInvalid -> reactory.dispatch(NotFoundRouterAction(joinToString("/") { it.asString() }))
                is RouteItemValid -> reactory.dispatch(ApplyActionWithoutBuildIns(route.action))
            } }
        )
    }

    private fun Collection<RouteItem>.removeAllBeforePrimaryAction(): List<RouteItem> =
        mutableListOf<RouteItem>().also { stack ->
            forEach { route -> when (route) {
                is RouteItemInvalid -> stack.add(route)
                is RouteItemValid -> {
                    if (route.action.isPrimaryAction()) {
                        stack.clear()
                    }
                    stack.add(route)
                }
            } }
        }

    private fun Collection<RouteItem>.retainOnlyValidRouteItems(): List<RouteItem> =
        filterIsInstance<RouteItemValid>()

    private fun Collection<RouteItem>.applyRoutesToBrowser(): Unit =
        this.joinToString("/") { it.asString() }
            .let { url -> when (mode) {
                RouterMode.HASH -> {
                    val title = window.document.title
                    val pathname = window.location.pathname.ifEmpty { "/" }
                    val search = window.location.search
                    val hash = "#$url"
                    val actualUrl = pathname + search + hash
                    window.history.pushState(null, title, actualUrl)
                }
                RouterMode.PATHNAME -> {
                    val title = window.document.title
                    val pathname = url.ifEmpty { "/" }
                    val search = window.location.search
                    val hash = window.location.hash
                    val actualUrl = pathname + search + hash
                    window.history.pushState(null, title, actualUrl)
                }
            } }

    private fun getRoutesFromBrowser(): List<RouteItem> =
        when (mode) {
            RouterMode.HASH -> window.location.hash.removePrefix("#")
            RouterMode.PATHNAME -> window.location.pathname
        }
            .decodeURI()
            .removeLeadingSymbols("/")
            .removeTrailingSymbols("/")
            .let { "/$it" }
            .split("/")
            .map { it.trim() }
            .map { RouteItem.makeFromRoute(it) }

    private fun fetchPath(route: String): String =
        route
            .split(":")
            .map { it.trim() }
            .get(0)

    private fun fetchParamValues(route: String, param2descriptor: Map<String, FieldDescriptor<Any?>>): Map<String, Any> =
        route
            .splitToSequence(":")
            .map { it.trim() }
            .drop(1)
            .map { param -> param.split("=") }
            .filter { param -> param.size == 2 }
            .map { param -> param[0] to param[1] }
            .map { (name, value) ->
                val descriptor = param2descriptor[name]
                val deserialized = descriptor?.type?.fromString(value)
                if (deserialized == null) {
                    return@map null
                }
                name to deserialized
            }
            .filterNotNull()
            .toMap()

    private fun fillMissingValuesWithNulls(values: Map<String, Any>, param2descriptor: Map<String, FieldDescriptor<Any?>>): Map<String, Any?> =
        values + param2descriptor.keys
            .filterNot { values.containsKey(it) }
            .map { it to null }

    private fun makeActionForPath(path: String, param2value: Map<String, Any?>): Option<RouterAction> =
        path2action[path].toOption().flatMap { actionClass ->
            val action = actionClass.newRouterActionInstance(param2value)
            val allFieldsInitialized = action.rFields
                .all { (name, descriptor) ->
                    val valueSet = action.isFieldValueSet(name)
                    val canBeNull = descriptor.nullable == Nullable
                    valueSet || canBeNull
                }
            if (allFieldsInitialized) {
                action.some()
            } else {
                none()
            }
        }

    private fun makeActionParamDescriptors(path: String): Option<Map<String, FieldDescriptor<Any?>>> =
        path2action[path].toOption()
            .map { it.newRouterActionMetaInstance().getAccessibleFieldDescriptors() }

    private fun RouterAction.getAccessibleFieldNamesWithValues(): Map<String, Any> =
        getAccessibleFieldDescriptors()
            .map { (name, _) ->
                val value = getFieldValue(name)
                if (value != null) {
                    name to value
                } else null
            }
            .filterNotNull()
            .toMap()

    private fun RouterAction.getAccessibleFieldDescriptors(): Map<String, FieldDescriptor<Any?>> =
        rFields.unsafeCast<Map<String, FieldDescriptor<Any?>>>()

    private fun RouterAction.isPrimaryAction(): Boolean =
        rPrimary

    private fun RouterAction.isFieldValueSet(name: String): Boolean =
        getFieldValue(name).let { it != null && it !== undefined }

    private fun RouterAction.getFieldValue(name: String): Any? =
        try { asDynamic()[name] } catch (e: Exception) { null }

    @Suppress("UNUSED_ANONYMOUS_PARAMETER", "UNUSED_VARIABLE", "UNUSED_PARAMETER")
    private fun <T : RouterAction> KClass<T>.newRouterActionInstance(map: Map<String, Any?>): T {
        val ctor = js.asDynamic()
        return js("new ctor(map)") as T
    }

    @Suppress("UNUSED_ANONYMOUS_PARAMETER", "UNUSED_VARIABLE")
    private fun <T : RouterAction> KClass<T>.newRouterActionMetaInstance(): RouterAction {
        val ctor = js.asDynamic()
        return js("new ctor()") as RouterAction
    }

    private fun String.normalizeAsPath(): String =
        this.removeLeadingSymbols("/")
            .removeTrailingSymbols("/")
            .trim()
            .also { if (it.contains("/")) throw IllegalArgumentException("Got path with '/' symbol : $this") }

    private fun String.removeLeadingSymbols(symbol: String): String =
        run {
            var str = this
            while (str.startsWith(symbol)) str = str.drop(1)
            str
        }

    private fun String.removeTrailingSymbols(symbol: String): String =
        run {
            var str = this
            while (str.endsWith(symbol)) str = str.dropLast(1)
            str
        }

}

private sealed class RouteItem { companion object }

private data class RouteItemInvalid(
    val route: String
) : RouteItem()

private data class RouteItemValid(
    val path: String,
    val param2value: Map<String, Any>,
    val param2descriptor: Map<String, FieldDescriptor<Any?>>,
    val action: RouterAction
) : RouteItem()
