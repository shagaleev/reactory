package ru.brainworm.reactory.core.descriptor

interface Type<T> {
    fun asString(value: T): String?
    fun fromString(string: String): T?
}

abstract class TypeIterable<T : Any>(private val type: Type<T>, private val separator: String = ",") : Type<Iterable<T>> {
    override fun asString(value: Iterable<T>): String? = value
        .mapNotNull { type.asString(it) }
        .joinToString(separator)
    override fun fromString(string: String): Iterable<T>? = string
        .split(separator)
        .filterNot { it.isEmpty() }
        .mapNotNull { type.fromString(it) }
}

abstract class TypeArray<T : Any>(private val type: TypeIterable<T>) : Type<Array<T>> {
    override fun asString(value: Array<T>): String? = type.asString(value.asIterable())
    override fun fromString(string: String): Array<T>? = type.fromString(string)?.toList()?.toTypedArray()
}

class TypeString : Type<String> {
    override fun asString(value: String): String? = value
    override fun fromString(string: String): String? = string
}
class TypeIterableString : TypeIterable<String>(TypeString())
class TypeArrayString : TypeArray<String>(TypeIterableString())
fun typeString(): FieldDescriptor<String> = FieldDescriptor(TypeString(), NullSafe)
fun typeIterableString(): FieldDescriptor<Iterable<String>> = FieldDescriptor(TypeIterableString(), NullSafe)
fun typeArrayString(): FieldDescriptor<Array<String>> = FieldDescriptor(TypeArrayString(), NullSafe)

class TypeBoolean : Type<Boolean> {
    override fun asString(value: Boolean): String? = if (value) "true" else "false"
    override fun fromString(string: String): Boolean? = string.toLowerCase() == "true"
}
class TypeIterableBoolean : TypeIterable<Boolean>(TypeBoolean())
class TypeArrayBoolean : TypeArray<Boolean>(TypeIterableBoolean())
fun typeBoolean(): FieldDescriptor<Boolean> = FieldDescriptor(TypeBoolean(), NullSafe)
fun typeIterableBoolean(): FieldDescriptor<Iterable<Boolean>> = FieldDescriptor(TypeIterableBoolean(), NullSafe)
fun typeArrayBoolean(): FieldDescriptor<Array<Boolean>> = FieldDescriptor(TypeArrayBoolean(), NullSafe)

class TypeByte : Type<Byte> {
    override fun asString(value: Byte): String? = value.toString()
    override fun fromString(string: String): Byte? = string.toByteOrNull()
}
class TypeIterableByte : TypeIterable<Byte>(TypeByte())
class TypeArrayByte : TypeArray<Byte>(TypeIterableByte())
fun typeByte(): FieldDescriptor<Byte> = FieldDescriptor(TypeByte(), NullSafe)
fun typeIterableByte(): FieldDescriptor<Iterable<Byte>> = FieldDescriptor(TypeIterableByte(), NullSafe)
fun typeArrayByte(): FieldDescriptor<Array<Byte>> = FieldDescriptor(TypeArrayByte(), NullSafe)

class TypeShort : Type<Short> {
    override fun asString(value: Short): String? = value.toString()
    override fun fromString(string: String): Short? = string.toShortOrNull()
}
class TypeIterableShort : TypeIterable<Short>(TypeShort())
class TypeArrayShort : TypeArray<Short>(TypeIterableShort())
fun typeShort(): FieldDescriptor<Short> = FieldDescriptor(TypeShort(), NullSafe)
fun typeIterableShort(): FieldDescriptor<Iterable<Short>> = FieldDescriptor(TypeIterableShort(), NullSafe)
fun typeArrayShort(): FieldDescriptor<Array<Short>> = FieldDescriptor(TypeArrayShort(), NullSafe)

class TypeInt : Type<Int> {
    override fun asString(value: Int): String? = value.toString()
    override fun fromString(string: String): Int? = string.toIntOrNull()
}
class TypeIterableInt : TypeIterable<Int>(TypeInt())
class TypeArrayInt : TypeArray<Int>(TypeIterableInt())
fun typeInt(): FieldDescriptor<Int> = FieldDescriptor(TypeInt(), NullSafe)
fun typeIterableInt(): FieldDescriptor<Iterable<Int>> = FieldDescriptor(TypeIterableInt(), NullSafe)
fun typeArrayInt(): FieldDescriptor<Array<Int>> = FieldDescriptor(TypeArrayInt(), NullSafe)

class TypeLong : Type<Long> {
    override fun asString(value: Long): String? = value.toString()
    override fun fromString(string: String): Long? = string.toLongOrNull()
}
class TypeIterableLong : TypeIterable<Long>(TypeLong())
class TypeArrayLong : TypeArray<Long>(TypeIterableLong())
fun typeLong(): FieldDescriptor<Long> = FieldDescriptor(TypeLong(), NullSafe)
fun typeIterableLong(): FieldDescriptor<Iterable<Long>> = FieldDescriptor(TypeIterableLong(), NullSafe)
fun typeArrayLong(): FieldDescriptor<Array<Long>> = FieldDescriptor(TypeArrayLong(), NullSafe)

class TypeFloat : Type<Float> {
    override fun asString(value: Float): String? = value.toString()
    override fun fromString(string: String): Float? = string.toFloatOrNull()
}
class TypeIterableFloat : TypeIterable<Float>(TypeFloat())
class TypeArrayFloat : TypeArray<Float>(TypeIterableFloat())
fun typeFloat(): FieldDescriptor<Float> = FieldDescriptor(TypeFloat(), NullSafe)
fun typeIterableFloat(): FieldDescriptor<Iterable<Float>> = FieldDescriptor(TypeIterableFloat(), NullSafe)
fun typeArrayFloat(): FieldDescriptor<Array<Float>> = FieldDescriptor(TypeArrayFloat(), NullSafe)

class TypeDouble : Type<Double> {
    override fun asString(value: Double): String? = value.toString()
    override fun fromString(string: String): Double? = string.toDoubleOrNull()
}
class TypeIterableDouble : TypeIterable<Double>(TypeDouble())
class TypeArrayDouble : TypeArray<Double>(TypeIterableDouble())
fun typeDouble(): FieldDescriptor<Double> = FieldDescriptor(TypeDouble(), NullSafe)
fun typeIterableDouble(): FieldDescriptor<Iterable<Double>> = FieldDescriptor(TypeIterableDouble(), NullSafe)
fun typeArrayDouble(): FieldDescriptor<Array<Double>> = FieldDescriptor(TypeArrayDouble(), NullSafe)
