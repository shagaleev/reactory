package ru.brainworm.reactory.core.logger.appender

import ru.brainworm.reactory.core.logger.LogLevel

open class ConsoleLogAppender : LogAppender {

    override fun onLog(level: Int, message: String, e: Throwable?) {
        when {
            level >= LogLevel.error() -> onError(message, e)
            level >= LogLevel.warn() -> onWarn(message, e)
            level >= LogLevel.info() -> onInfo(message, e)
            level >= LogLevel.debug() -> onDebug(message, e)
            level >= LogLevel.trace() -> onTrace(message, e)
            else -> onDebug(message, e)
        }
    }

    private fun onTrace(message: String, e: Throwable?) {
        if (e == null) {
            console.log(message)
        } else {
            console.log(message, e)
        }
    }

    private fun onDebug(message: String, e: Throwable?) {
        if (e == null) {
            console.log(message)
        } else {
            console.log(message, e)
        }
    }

    private fun onInfo(message: String, e: Throwable?) {
        if (e == null) {
            console.info(message)
        } else {
            console.info(message, e)
        }
    }

    private fun onWarn(message: String, e: Throwable?) {
        if (e == null) {
            console.warn(message)
        } else {
            console.warn(message, e)
        }
    }

    private fun onError(message: String, e: Throwable?) {
        if (e == null) {
            console.error(message)
        } else {
            console.error(message, e)
        }
    }
}
