@file:Suppress("OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS")
package org.w3c.dom

import org.w3c.dom.events.Event
import org.w3c.dom.events.EventListener
import org.w3c.dom.events.EventTarget

// NOTE: THIS FILE IS AUTO-GENERATED
// See github.com/kotlin/dukat for details

/**
 * Exposes the JavaScript [AbortController](https://developer.mozilla.org/ru/docs/Web/API/AbortController) to Kotlin
 */
external class AbortController {
    var signal: AbortSignal
    fun abort()
}

/**
 * Exposes the JavaScript [AbortSignal](https://developer.mozilla.org/ru/docs/Web/API/AbortSignal) to Kotlin
 */
external class AbortSignal : EventTarget {
    var aborted: Boolean
    var onabort: ((self: AbortSignal, ev: Event) -> Any)?
    fun <K : Any> addEventListener(type: K, listener: (self: AbortSignal, ev: Any) -> Any, options: Boolean = definedExternally)
    fun <K : Any> addEventListener(type: K, listener: (self: AbortSignal, ev: Any) -> Any, options: AddEventListenerOptions = definedExternally)
    fun addEventListener(type: String, listener: EventListener, options: Boolean = definedExternally)
    override fun addEventListener(type: String, callback: EventListener?, options: Boolean)
    fun addEventListener(type: String, listener: EventListener, options: AddEventListenerOptions = definedExternally)
    override fun addEventListener(type: String, callback: EventListener?, options: AddEventListenerOptions)
    fun addEventListener(type: String, listener: EventListenerObject, options: Boolean = definedExternally)
    fun addEventListener(type: String, listener: EventListenerObject?, options: Boolean)
    fun addEventListener(type: String, listener: EventListenerObject, options: AddEventListenerOptions = definedExternally)
    fun addEventListener(type: String, listener: EventListenerObject?, options: AddEventListenerOptions)
    fun <K : Any> removeEventListener(type: K, listener: (self: AbortSignal, ev: Any) -> Any, options: Boolean = definedExternally)
    fun <K : Any> removeEventListener(type: K, listener: (self: AbortSignal, ev: Any) -> Any, options: EventListenerOptions = definedExternally)
    fun removeEventListener(type: String, listener: EventListener, options: Boolean = definedExternally)
    override fun removeEventListener(type: String, callback: EventListener?, options: Boolean)
    fun removeEventListener(type: String, listener: EventListener, options: EventListenerOptions = definedExternally)
    override fun removeEventListener(type: String, callback: EventListener?, options: EventListenerOptions)
    fun removeEventListener(type: String, listener: EventListenerObject, options: Boolean = definedExternally)
    fun removeEventListener(type: String, callback: EventListenerObject?, options: Boolean)
    fun removeEventListener(type: String, listener: EventListenerObject, options: EventListenerOptions = definedExternally)
    fun removeEventListener(type: String, callback: EventListenerObject?, options: EventListenerOptions)
}

external interface EventListenerObject {
    fun handleEvent(evt: Event)
}
