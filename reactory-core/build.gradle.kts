
dependencies {
    api(kotlin("stdlib-js"))
    api("org.jetbrains:kotlin-react:${Version.Kotlin.react}")
    api("org.jetbrains:kotlin-react-dom:${Version.Kotlin.react}")
    api("org.jetbrains:kotlin-styled:${Version.Kotlin.styled}")
    api("org.jetbrains.kotlinx:kotlinx-serialization-json:${Version.KotlinX.serialization}")
    api(npm("react", Version.Npm.react))
    api(npm("react-dom", Version.Npm.react))
    api(npm("core-js", Version.Npm.Polyfill.coreJs))
    api(npm("whatwg-fetch", Version.Npm.Polyfill.whatwgFetch))
    api(npm("abortcontroller-polyfill", Version.Npm.Polyfill.abortController))
}
