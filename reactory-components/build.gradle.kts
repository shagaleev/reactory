
dependencies {
    api(project(":reactory-core"))
    api(npm("resize-observer-polyfill", Version.Npm.Polyfill.resizeObserver))
}
