package ru.brainworm.reactory.components.window

internal const val ITEM_ESTIMATED_HEIGHT_DEFAULT: Double = 50.0
internal const val IS_SCROLLING_DEBOUNCE_INTERVAL: Double = 400.0
