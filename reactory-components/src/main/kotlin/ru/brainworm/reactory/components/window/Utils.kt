package ru.brainworm.reactory.components.window

import react.ReactElement
import kotlin.math.max
import kotlin.math.min

internal fun checkWindowItemChildren(children: Any) {
    if (children == undefined) {
        throw error("Expected single ReactElement at WindowProps.children")
    }
    val childrenElement = children.unsafeCast<ReactElement>()
    if (childrenElement.props == undefined) {
        throw error("Expected single ReactElement at WindowProps.children")
    }
}

internal fun findNearestIndexExponentialSearch(
    index: Int,
    offset: Double,
    size: Int,
    getOffsetForIndex: (Int) -> Double
): Int {
    var mIndex = index
    var mInterval = 1
    while (
        mIndex < size &&
        getOffsetForIndex(mIndex) < offset
    ) {
        mIndex += mInterval
        mInterval *= 2
    }
    return findNearestIndexBinarySearch(
        high = min(mIndex, size - 1),
        low = mIndex / 2,
        offset = offset,
        getOffsetForIndex = getOffsetForIndex,
    )
}

internal fun findNearestIndexBinarySearch(
    high: Int,
    low: Int,
    offset: Double,
    getOffsetForIndex: (Int) -> Double
): Int {
    var mHigh = high
    var mLow = low
    while (mLow <= mHigh) {
        val middle = mLow + ((mHigh - mLow) / 2)
        val currentOffset = getOffsetForIndex(middle)
        when {
            currentOffset == offset -> return middle
            currentOffset < offset -> mLow = middle + 1
            currentOffset > offset -> mHigh = middle - 1
        }
    }
    return max(0, mLow - 1)
}
