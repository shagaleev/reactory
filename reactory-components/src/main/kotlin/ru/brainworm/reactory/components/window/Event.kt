package ru.brainworm.reactory.components.window

import org.w3c.dom.HTMLDivElement
import org.w3c.dom.events.Event
import react.SyntheticEvent

internal interface ScrollEvent : SyntheticEvent<HTMLDivElement, Event>
