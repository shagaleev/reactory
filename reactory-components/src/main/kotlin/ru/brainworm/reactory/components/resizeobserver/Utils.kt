package ru.brainworm.reactory.components.resizeobserver

import org.w3c.dom.HTMLElement
import ru.brainworm.reactory.core.js.getWindow

internal fun getContentRect(
    element: HTMLElement,
    types: Array<ContentRectType>,
): ContentRect =
    ContentRect(
        client = when {
            ContentRectType.CLIENT in types -> ContentRectClient(
                top = element.clientTop.toDouble(),
                left = element.clientLeft.toDouble(),
                width = element.clientWidth.toDouble(),
                height = element.clientHeight.toDouble(),
            )
            else -> null
        },
        offset = when {
            ContentRectType.OFFSET in types -> ContentRectOffset(
                top = element.offsetTop.toDouble(),
                left = element.offsetLeft.toDouble(),
                width = element.offsetWidth.toDouble(),
                height = element.offsetHeight.toDouble(),
            )
            else -> null
        },
        scroll = when {
            ContentRectType.SCROLL in types -> ContentRectScroll(
                top = element.scrollTop,
                left = element.scrollLeft,
                width = element.scrollWidth.toDouble(),
                height = element.scrollHeight.toDouble(),
            )
            else -> null
        },
        bounds = when {
            ContentRectType.BOUNDS in types -> {
                val rect = element.getBoundingClientRect()
                ContentRectBounds(
                    top = rect.top,
                    right = rect.right,
                    bottom = rect.bottom,
                    left = rect.left,
                    width = rect.width,
                    height = rect.height,
                )
            }
            else -> null
        },
        margin = when {
            ContentRectType.MARGIN in types -> {
                val styles = element.getWindow().getComputedStyle(element)
                ContentRectMargin(
                    top = styles.marginTop.toDoubleOrNull() ?: 0.0,
                    right = styles.marginRight.toDoubleOrNull() ?: 0.0,
                    bottom = styles.marginBottom.toDoubleOrNull() ?: 0.0,
                    left = styles.marginLeft.toDoubleOrNull() ?: 0.0,
                )
            }
            else -> null
        },
        entry = null,
    )

internal fun collectTypes(
    props: ResizeObserverProps,
): Array<ContentRectType> =
    props.types ?: listOfNotNull(
        ContentRectType.CLIENT.takeIf { props.client == true },
        ContentRectType.OFFSET.takeIf { props.offset == true },
        ContentRectType.SCROLL.takeIf { props.scroll == true },
        ContentRectType.BOUNDS.takeIf { props.bounds == true },
        ContentRectType.MARGIN.takeIf { props.margin == true },
    ).toTypedArray()
