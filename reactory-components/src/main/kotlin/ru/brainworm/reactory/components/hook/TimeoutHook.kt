package ru.brainworm.reactory.components.hook

import kotlinx.browser.window
import react.useCallback
import react.useEffectWithCleanup
import react.useRef
import kotlin.js.Date

typealias RequestTimeoutFunction = (delay: Double, callback: () -> Unit) -> Unit
typealias CancelTimeoutFunction = () -> Unit
data class TimeoutHook(
    val requestTimeout: RequestTimeoutFunction,
    val cancelTimeout: CancelTimeoutFunction,
)

fun useTimeoutHook(): TimeoutHook {
    val cancelTimeoutFunction = useRef<CancelTimeoutFunction?>(null)
    val cancelTimeout = useCallback({
        cancelTimeoutFunction.current?.invoke()
        cancelTimeoutFunction.current = null
        Unit
    }, listOf())
    val requestTimeout = useCallback({ delay: Double, callback: () -> Unit ->
        cancelTimeout()
        cancelTimeoutFunction.current = requestTimeout(delay) {
            cancelTimeoutFunction.current = null
            callback()
        }
        Unit
    }, listOf())

    useEffectWithCleanup({
        return@useEffectWithCleanup {
            cancelTimeoutFunction.current?.invoke()
        }
    }, listOf())

    return TimeoutHook(
        requestTimeout = requestTimeout,
        cancelTimeout = cancelTimeout,
    )
}

private fun requestTimeout(delay: Double, onExpired: () -> Unit): CancelTimeoutFunction {
    val start = Date.now()
    var timeoutId: Int?
    timeoutId = window.requestAnimationFrame {
        timeoutId = tick(start, delay, onExpired)
    }
    return {
        timeoutId?.let(::cancelTimeout)
    }
}

private fun tick(start: Double, delay: Double, onExpired: () -> Unit): Int? {
    return when {
        Date.now() - start >= delay -> {
            onExpired()
            null
        }
        else -> window.requestAnimationFrame { tick(start, delay, onExpired) }
    }
}

private fun cancelTimeout(timeoutId: Int) {
    window.cancelAnimationFrame(timeoutId)
}
