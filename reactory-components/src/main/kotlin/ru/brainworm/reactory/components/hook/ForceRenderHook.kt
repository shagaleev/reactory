package ru.brainworm.reactory.components.hook

import react.useCallback
import react.useReducer

typealias ForceRenderFunction = () -> Unit

fun useForceRender(): ForceRenderFunction {
    val (_, forceRender) = useReducer<Int, Unit>({ value, _ ->
        when {
            value > Int.MAX_VALUE - 10 -> 0
            else -> value + 1
        }
    }, 0)
    return useCallback({ forceRender(Unit) }, listOf())
}
