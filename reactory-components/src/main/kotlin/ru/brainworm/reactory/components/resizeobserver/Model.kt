package ru.brainworm.reactory.components.resizeobserver

import org.w3c.dom.DOMRectReadOnly

enum class ContentRectType {
    CLIENT,
    OFFSET,
    SCROLL,
    BOUNDS,
    MARGIN,
    ;
}

data class ContentRect(
    val client: ContentRectClient?,
    val offset: ContentRectOffset?,
    val scroll: ContentRectScroll?,
    val bounds: ContentRectBounds?,
    val margin: ContentRectMargin?,
    val entry: DOMRectReadOnly?,
)

data class ContentRectClient(
    val top: Double,
    val left: Double,
    val width: Double,
    val height: Double,
)

data class ContentRectOffset(
    val top: Double,
    val left: Double,
    val width: Double,
    val height: Double,
)

data class ContentRectScroll(
    val top: Double,
    val left: Double,
    val width: Double,
    val height: Double,
)

data class ContentRectBounds(
    val top: Double,
    val right: Double,
    val bottom: Double,
    val left: Double,
    val width: Double,
    val height: Double,
)

data class ContentRectMargin(
    val top: Double,
    val right: Double,
    val bottom: Double,
    val left: Double,
)
