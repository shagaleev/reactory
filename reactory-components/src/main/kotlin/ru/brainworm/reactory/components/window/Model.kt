package ru.brainworm.reactory.components.window

enum class ScrollAnchor {
    OUTER_ELEMENT,
    WINDOW,
    ;
}

enum class ScrollAlign {
    AUTO,
    START,
    CENTER,
    END,
    ;
}

enum class ScrollDirection {
    FORWARD,
    BACKWARD,
}

internal data class RangeToRender(
    val startIndex: Int,
    val stopIndex: Int,
)
