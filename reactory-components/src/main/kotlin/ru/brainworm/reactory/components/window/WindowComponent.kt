package ru.brainworm.reactory.components.window

import kotlinext.js.js
import kotlinext.js.jsObject
import org.w3c.dom.Element
import org.w3c.dom.HTMLElement
import org.w3c.dom.Window
import org.w3c.dom.events.EventListener
import react.*
import ru.brainworm.reactory.components.hook.useForceRender
import ru.brainworm.reactory.components.hook.useTimeoutHook
import ru.brainworm.reactory.components.resizeobserver.*
import ru.brainworm.reactory.core.js.getWindow
import kotlin.math.max
import kotlin.math.min
import kotlin.math.round

fun RBuilder.window(
    classes: String? = null,
    handler: RHandler<WindowProps> = {},
): ReactElement =
    child(windowComponent) {
        attrs.outerTagClasses = classes
        handler()
    }

external interface WindowProps : RProps {
    var windowRef: RMutableRef<WindowRef>?
    var itemCount: (() -> Int)?
    var itemEstimatedHeight: Double?
    var itemKeyByIndex: ((Int) -> String?)?
    var itemIndexByKey: ((String) -> Int?)?
    var itemResizeObserverTypes: Array<ContentRectType>?
    var itemHeightMeasurer: ((ContentRect) -> Double?)?
    var offsetOverscanBackward: ((scrollOffset: Double, isScrolling: Boolean, scrollDirection: ScrollDirection) -> Double)?
    var offsetOverscanForward: ((scrollOffset: Double, isScrolling: Boolean, scrollDirection: ScrollDirection) -> Double)?
    var outerTagClasses: String?
    var outerTagName: String?
    var outerTagRef: ((Element?) -> Unit)?
    var innerTagClasses: String?
    var innerTagName: String?
    var innerTagRef: ((Element?) -> Unit)?
    var scrollAnchor: ScrollAnchor?
}

interface WindowRef {
    var scrollToOffset: (offset: Double) -> Unit
    var scrollToItemKey: (itemKey: String, scrollAlign: ScrollAlign) -> Unit
    var resetAfterItemKey: (itemKey: String) -> Unit
}

external interface WindowItemProps : RProps {
    var index: Int
    var style: Any
    var itemRef: (element: HTMLElement?) -> Unit
}

val windowComponent = functionalComponent<WindowProps>("window") { props ->
    checkWindowItemChildren(props.children)

    val forceRender = useForceRender()
    val (isScrolling, setScrolling) = useState(false)
    val (scrollDirection, setScrollDirection) = useState(ScrollDirection.FORWARD)
    val (scrollOffset, setScrollOffset) = useState(0.0)
    val (requestScrollDebounceTimeout, _) = useTimeoutHook()
    val (scrollUpdateWasRequested, requestScrollUpdate) = useState(false)
    val outerTagRef = useRef<HTMLElement?>(null)
    val outerTagClientHeightRef = useRef<Double?>(null)
    val outerTagOffsetTopRef = useRef<Double?>(null)
    val itemKey2Height = useRef<MutableMap<String, Double>>(mutableMapOf())

    val getItemCount = useCallback({
        props.itemCount?.invoke() ?: 0
    }, listOf(props.itemCount))

    val getItemKey = useCallback({ index: Int ->
        props.itemKeyByIndex?.invoke(index)
            ?: error("WindowProps.itemKeyByIndex should return valid key")
    }, listOf(props.itemKeyByIndex))

    val getItemIndex = useCallback({ key: String ->
        props.itemIndexByKey?.invoke(key)
            ?: error("WindowProps.itemIndexByKey should return valid index")
    }, listOf(props.itemIndexByKey))

    val outerTagRefSetter = useCallback({ element: HTMLElement? ->
        outerTagRef.current = element
        props.outerTagRef?.invoke(element)
    }, listOf(props.outerTagRef))

    val innerTagRefSetter = useCallback({ element: Element? ->
        props.innerTagRef?.invoke(element)
    }, listOf(props.innerTagRef))

    val getBrowserWindowHeight = useCallback({ element: Element? ->
        element.getWindow().innerHeight.toDouble()
    }, listOf())

    val getOuterElementHeight = useCallback({
        when (props.scrollAnchor) {
            ScrollAnchor.WINDOW ->
                getBrowserWindowHeight(outerTagRef.current)
            ScrollAnchor.OUTER_ELEMENT, null ->
                outerTagClientHeightRef.current
                    ?: getBrowserWindowHeight(outerTagRef.current)
        }
    }, listOf(props.scrollAnchor, outerTagClientHeightRef.current, getBrowserWindowHeight))

    val getItemEstimatedHeight = useCallback({
        when {
            itemKey2Height.current.isEmpty() -> props.itemEstimatedHeight
                ?: ITEM_ESTIMATED_HEIGHT_DEFAULT
            else -> itemKey2Height.current.values
                .sum()
                .div(itemKey2Height.current.size.toDouble())
        }
    }, listOf(itemKey2Height.current, props.itemEstimatedHeight))

    val resetAfterItemKey = useCallback({ itemKey: String ->
        if (getItemCount() <= 0) {
            return@useCallback
        }
        val itemIndex = getItemIndex(itemKey)
        val fromIndex = max(0, itemIndex + 1)
        val toIndex = getItemCount() - 1
        (fromIndex..toIndex)
            .map(getItemKey)
            .forEach { itemKey2Height.current.remove(it) }
        forceRender()
        Unit
    }, listOf(getItemCount, getItemKey, getItemIndex, itemKey2Height.current))

    val setItemHeight = useCallback({ key: String, height: Double? ->
        val heightOld = itemKey2Height.current[key]
        when (height) {
            null -> itemKey2Height.current.remove(key)
            else -> itemKey2Height.current[key] = height
        }
        if (heightOld != height) {
            forceRender()
        }
        Unit
    }, listOf(itemKey2Height.current))

    val getItemHeight = useCallback({ key: String ->
        val height = itemKey2Height.current[key]
            ?: getItemEstimatedHeight()
        height
    }, listOf(itemKey2Height.current, getItemEstimatedHeight))

    val getItemOffset = useCallback({ key: String ->
        val index = getItemIndex(key)
        val offset = (0 until index)
            .map(getItemKey)
            .map(getItemHeight)
            .sum()
        offset
    }, listOf(getItemIndex, getItemKey, getItemHeight))

    val getEstimatedTotalHeight = useCallback({
        if (getItemCount() <= 0) {
            return@useCallback 0.0
        }
        val lastItemIndex = getItemCount() - 1
        val lastItemKey = getItemKey(lastItemIndex)
        val lastItemOffset = getItemOffset(lastItemKey)
        val lastItemHeight = getItemHeight(lastItemKey)
        lastItemOffset + lastItemHeight
    }, listOf(getItemCount, getItemKey, getItemOffset, getItemHeight))

    val getWindowScrollOffsetTopPosition = useCallback({ window: Window ->
        window.pageYOffset.takeIf { it > 0.0 }
            ?: window.document.documentElement?.scrollTop?.takeIf { it > 0.0 }
            ?: window.document.body?.scrollTop?.takeIf { it > 0.0 }
            ?: 0.0
    }, listOf())

    val requestScrollDebounce = useCallback({
        requestScrollDebounceTimeout(IS_SCROLLING_DEBOUNCE_INTERVAL) {
            setScrolling(false)
        }
    }, listOf())

    val onOuterTagResize = useCallback({ rect: ContentRect ->
        outerTagClientHeightRef.current = rect.client?.height
        outerTagOffsetTopRef.current = rect.offset?.top
    }, listOf())

    val onItemResize = useCallback({ key: String, rect: ContentRect ->
        val height = props.itemHeightMeasurer?.invoke(rect)
            ?: rect.offset?.height
        setItemHeight(key, height)
    }, listOf(props.itemHeightMeasurer, setItemHeight))

    val onScroll = useCallback({ event: ScrollEvent ->
        val isScrollEventAtRootElement = event.currentTarget == event.target
        if (!isScrollEventAtRootElement) {
            return@useCallback
        }
        val target = event.currentTarget
        val clientHeight = target.clientHeight
        val scrollHeight = target.scrollHeight
        val scrollTop = target.scrollTop
        if (scrollOffset == scrollTop) {
            return@useCallback
        }
        val scrollOffsetNew = max(
            0.0,
            min(scrollTop, (scrollHeight - clientHeight).toDouble())
        )
        setScrolling(true)
        setScrollDirection(when {
            scrollOffset < scrollOffsetNew -> ScrollDirection.FORWARD
            else -> ScrollDirection.BACKWARD
        })
        setScrollOffset(scrollOffsetNew)
        requestScrollDebounce()
    }, listOf(scrollOffset, requestScrollDebounce))

    val getStartIndexForOffset = useCallback({ offset: Double ->
        findNearestIndexExponentialSearch(
            index = 0,
            size = getItemCount(),
            offset = offset,
            getOffsetForIndex = { getItemOffset(getItemKey(it)) },
        )
    }, listOf(getItemOffset, getItemKey, getItemCount))

    val getStopIndexForStartIndex = useCallback({ startIndex: Int, offset: Double ->
        val height = getOuterElementHeight()
        val maxOffset = offset + height
        val startKey = getItemKey(startIndex)
        var itemOffset = getItemOffset(startKey) + getItemHeight(startKey)
        var stopIndex = startIndex
        while (
            stopIndex < getItemCount() - 1 &&
            itemOffset < maxOffset
        ) {
            stopIndex++
            itemOffset += getItemHeight(getItemKey(stopIndex))
        }
        stopIndex
    }, listOf(getItemCount, getItemKey, getItemOffset, getItemHeight, getOuterElementHeight, outerTagRef.current))

    val offsetOverscanBackward = useCallback({ offset: Double ->
        props.offsetOverscanBackward?.invoke(offset, isScrolling, scrollDirection) ?: {
            when {
                isScrolling && scrollDirection == ScrollDirection.BACKWARD -> offset - (getOuterElementHeight() * 0.5)
                else -> offset
            }
        }()
    }, listOf(
        props.offsetOverscanBackward, getOuterElementHeight,
        isScrolling, scrollDirection,
    ))

    val offsetOverscanForward = useCallback({ offset: Double ->
        props.offsetOverscanForward?.invoke(offset, isScrolling, scrollDirection) ?: {
            when {
                isScrolling && scrollDirection == ScrollDirection.FORWARD -> offset + (getOuterElementHeight() * 0.5)
                else -> offset
            }
        }()
    }, listOf(
        props.offsetOverscanForward, getOuterElementHeight,
        isScrolling, scrollDirection,
    ))

    val getRangeToRender = useCallback({
        if (getItemCount() <= 0) {
            return@useCallback RangeToRender(0, 0)
        }
        val startOffsetOverscan = offsetOverscanBackward(scrollOffset)
        val stopOffsetOverscan = offsetOverscanForward(scrollOffset)
        val startIndex = getStartIndexForOffset(startOffsetOverscan)
        val stopIndex = getStopIndexForStartIndex(startIndex, stopOffsetOverscan)
        RangeToRender(startIndex, stopIndex)
    }, listOf(
        scrollOffset, offsetOverscanBackward, offsetOverscanForward,
        getItemCount, getStartIndexForOffset, getStopIndexForStartIndex,
    ))

    val getItemStyle = useCallback({ key: String ->
        val offset = getItemOffset(key)
        js {
            this.position = "absolute"
            this.top = offset
        } as Any
    }, listOf(getItemOffset))

    val scrollToOffset = useCallback({ offset: Double ->
        val outerElementHeight = getOuterElementHeight()
        val totalHeight = getEstimatedTotalHeight()
        val scrollOffsetMax = totalHeight - outerElementHeight
        val scrollOffsetNew = max(0.0, min(offset, scrollOffsetMax))
        setScrolling(true)
        setScrollDirection(when {
            scrollOffset < scrollOffsetNew -> ScrollDirection.FORWARD
            else -> ScrollDirection.BACKWARD
        })
        setScrollOffset(scrollOffsetNew)
        requestScrollDebounce()
        Unit
    }, listOf(scrollOffset, getOuterElementHeight, getEstimatedTotalHeight, requestScrollDebounce))

    val scrollToOffsetWithScrollUpdate = useCallback({ offset: Double ->
        scrollToOffset(offset)
        requestScrollUpdate(true)
        Unit
    }, listOf(scrollToOffset, requestScrollUpdate))

    val scrollToItemKey = useCallback({ itemKey: String, scrollAlign: ScrollAlign ->
        val itemHeight = getItemHeight(itemKey)
        val itemOffset = getItemOffset(itemKey)
        val outerElementHeight = getOuterElementHeight()
        val estimatedTotalHeight = getEstimatedTotalHeight()
        val minOffset = max(0.0, itemOffset - outerElementHeight + itemHeight)
        val maxOffset = max(0.0, min(estimatedTotalHeight - outerElementHeight, itemOffset))
        val scrollOffsetNew = when (scrollAlign) {
            ScrollAlign.START -> maxOffset
            ScrollAlign.END -> minOffset
            ScrollAlign.CENTER -> round(minOffset + (maxOffset - minOffset) / 2.0)
            ScrollAlign.AUTO -> when {
                scrollOffset in minOffset..maxOffset -> scrollOffset
                scrollOffset < minOffset -> minOffset
                else -> maxOffset
            }
        }
        scrollToOffsetWithScrollUpdate(scrollOffsetNew)
        Unit
    }, listOf(scrollToOffsetWithScrollUpdate, scrollOffset, getEstimatedTotalHeight, getItemHeight, getItemOffset, getOuterElementHeight))

    useEffect({
        if (!scrollUpdateWasRequested) {
            return@useEffect
        }
        when (props.scrollAnchor) {
            ScrollAnchor.OUTER_ELEMENT, null -> {
                outerTagRef.current?.scrollTop = scrollOffset
            }
            ScrollAnchor.WINDOW -> {
                val window = outerTagRef.current.getWindow()
                val extraOffsetTop = outerTagOffsetTopRef.current ?: 0.0
                val scrollOffsetNext = extraOffsetTop + scrollOffset
                window.scrollTo(window.scrollX, scrollOffsetNext)
            }
        }
        requestScrollUpdate(false)
    }, listOf(
        props.scrollAnchor, outerTagRef.current, outerTagOffsetTopRef.current,
        scrollUpdateWasRequested, scrollOffset,
    ))

    useEffect({
        if (props.windowRef != null) {
            props.windowRef!!.current = object : WindowRef {
                override var scrollToOffset = scrollToOffsetWithScrollUpdate
                override var scrollToItemKey = scrollToItemKey
                override var resetAfterItemKey = resetAfterItemKey
            }
        }
    }, listOf(props.windowRef, scrollToOffsetWithScrollUpdate, scrollToItemKey, resetAfterItemKey))

    useEffectWithCleanup({
        val window = outerTagRef.current.getWindow()
        val handleWindowScroll = EventListener { event ->
            val extraOffsetTop = outerTagOffsetTopRef.current ?: 0.0
            val scrollOffsetTop = getWindowScrollOffsetTopPosition(window)
            val scrollTop = scrollOffsetTop - extraOffsetTop
            scrollToOffset(scrollTop)
        }
        if (props.scrollAnchor == ScrollAnchor.WINDOW) {
            window.addEventListener("scroll", handleWindowScroll)
        }
        return@useEffectWithCleanup {
            window.removeEventListener("scroll", handleWindowScroll)
        }
    }, listOf(outerTagRef.current, outerTagOffsetTopRef.current, scrollToOffset, props.scrollAnchor, getWindowScrollOffsetTopPosition))

    val (startIndex, stopIndex) = getRangeToRender()
    val items: Array<ReactElement> = when {
        getItemCount() > 0 -> (startIndex..stopIndex).map { index ->
            val key = getItemKey(index)
            createElement(
                type = resizeObserverComponent,
                props = jsObject<ResizeObserverProps> {
                    this.key = key
                    this.offset = true
                    this.types = props.itemResizeObserverTypes
                    this.onResize = { onItemResize(key, it) }
                },
                { renderProp: ResizeObserverRenderProps ->
                    cloneElement(
                        element = props.children,
                        props = jsObject<WindowItemProps> {
                            this.key = key
                            this.index = index
                            this.style = getItemStyle(key)
                            this.itemRef = renderProp.measureRef
                        },
                    )
                }
            )
        }.toTypedArray()
        else -> emptyArray()
    }

    resizeObserver {
        attrs.client = true
        attrs.offset = props.scrollAnchor == ScrollAnchor.WINDOW
        attrs.onResize = onOuterTagResize
        childFunc { renderProp: ResizeObserverRenderProps ->
            createElement(
                type = props.outerTagName ?: "div",
                props = js {
                    this.ref = { outerElement: HTMLElement? ->
                        renderProp.measureRef(outerElement)
                        outerTagRefSetter(outerElement)
                    }
                    this.className = props.outerTagClasses ?: "reactory-window-outer"
                    this.onScroll = onScroll
                    this.style = js {
                        position = "relative"
                        WebkitOverflowScrolling = "touch"
                        willChange = "transform"
                    }
                },
                createElement(
                    type = props.innerTagName ?: "div",
                    props = js {
                        this.ref = innerTagRefSetter
                        this.children = items
                        this.className = props.innerTagClasses ?: "reactory-window-inner"
                        this.style = js {
                            this.height = getEstimatedTotalHeight()
                            this.width = "100%"
                            this.pointerEvents = "none".takeIf { isScrolling } ?: undefined
                        }
                    },
                )
            )
        }
    }
}
