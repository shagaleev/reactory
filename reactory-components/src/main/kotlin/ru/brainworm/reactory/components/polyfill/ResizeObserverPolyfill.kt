@file:JsModule("resize-observer-polyfill")
@file:JsNonModule
package ru.brainworm.reactory.components.polyfill

import org.w3c.dom.DOMRectReadOnly
import org.w3c.dom.Element

@JsName("default")
external class ResizeObserver(
    callback: (entries: Array<ResizeObserverEntry>, observer: ResizeObserver) -> Unit
) {
    fun observe(target: Element)
    fun unobserve(target: Element)
    fun disconnect()
}

external interface ResizeObserverEntry {
    var target: Element
    var contentRect: DOMRectReadOnly
}
