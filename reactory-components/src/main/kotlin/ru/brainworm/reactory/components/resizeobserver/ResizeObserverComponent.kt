package ru.brainworm.reactory.components.resizeobserver

import org.w3c.dom.HTMLElement
import react.*
import ru.brainworm.reactory.components.polyfill.ResizeObserver
import ru.brainworm.reactory.components.polyfill.ResizeObserverEntry

fun RBuilder.resizeObserver(
    handler: RHandler<ResizeObserverProps> = {},
): ReactElement =
    child(resizeObserverComponent) {
        handler()
    }

external interface ResizeObserverProps : RProps {
    var client: Boolean?
    var offset: Boolean?
    var scroll: Boolean?
    var bounds: Boolean?
    var margin: Boolean?
    var types: Array<ContentRectType>?
    var onResize: ((contentRect: ContentRect) -> Unit)?
}

interface ResizeObserverRenderProps {
    val measureRef: (element: HTMLElement?) -> Unit
}

val resizeObserverComponent = functionalComponent<ResizeObserverProps>("resizeObserver") { props ->

    val element = useRef<HTMLElement?>(null)
    val resizeObserver = useRef<ResizeObserver?>(null)

    val measureRef = useCallback({ node: HTMLElement? ->
        resizeObserver.current?.let { observer ->
            element.current?.let { element ->
                observer.unobserve(element)
            }
        }
        element.current = node
        resizeObserver.current?.let { observer ->
            element.current?.let { element ->
                observer.observe(element)
            }
        }
        Unit
    }, listOf(resizeObserver.current))

    val measure = useCallback({ entries: Array<ResizeObserverEntry>, observer: ResizeObserver ->
        val node = element.current
            ?: return@useCallback
        val contentRect = getContentRect(node, collectTypes(props))
            .let { contentRect -> when {
                entries.isNotEmpty() -> contentRect.copy(entry = entries[0].contentRect)
                else -> contentRect
            } }
        if (resizeObserver.current != null) {
            props.onResize?.invoke(contentRect)
        }
        Unit
    }, listOf(element.current, resizeObserver.current))

    useEffectWithCleanup({
        resizeObserver.current = ResizeObserver(measure)
        resizeObserver.current?.let { observer ->
            element.current?.let { element ->
                observer.observe(element)
            }
        }
        element.current?.let { element ->
            props.onResize?.invoke(getContentRect(element, collectTypes(props)))
        }
        return@useEffectWithCleanup {
            resizeObserver.current?.disconnect()
            resizeObserver.current = null
        }
    }, listOf())

    props.children(object : ResizeObserverRenderProps {
        override val measureRef = measureRef
    })
}
