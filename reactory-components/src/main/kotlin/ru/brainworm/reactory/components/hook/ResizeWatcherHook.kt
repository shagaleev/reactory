package ru.brainworm.reactory.components.hook

import org.w3c.dom.Element
import org.w3c.dom.events.EventListener
import react.RDependenciesList
import react.useEffectWithCleanup
import ru.brainworm.reactory.core.js.getWindow

typealias OnResizeFunction = () -> Unit

fun useResizeWatcher(
    element: Element? = null,
    dependencies: RDependenciesList = emptyList(),
    onResize: OnResizeFunction,
) {
    useEffectWithCleanup({
        val listener = EventListener { onResize() }
        val window = element.getWindow()
        window.addEventListener("resize", listener, true)
        return@useEffectWithCleanup {
            window.removeEventListener("resize", listener, true)
        }
    }, dependencies + element)
}
