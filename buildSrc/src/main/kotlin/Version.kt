object Version {
    const val kotlin = "1.4.10"

    object Kotlin {
        const val react = "${Npm.react}-pre.124-kotlin-$kotlin"
        const val styled = "5.2.0-pre.124-kotlin-$kotlin"
    }

    object KotlinX {
        const val serialization = "1.0.0"
    }

    object Npm {
        const val react = "16.13.1"
        object Polyfill {
            const val coreJs = "3.6.5"
            const val whatwgFetch = "3.0.0"
            const val abortController = "1.5.0"
            const val resizeObserver = "1.5.1"
        }
    }
}
