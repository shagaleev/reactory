plugins {
    kotlin("js") version Version.kotlin
    kotlin("plugin.serialization") version Version.kotlin
    `maven-publish`
}

kotlin { js { browser() } }

allprojects {
    group = "ru.brainworm.reactory"
    version = "1.0-SNAPSHOT"
    repositories {
        mavenCentral()
        jcenter()
        maven(url = "https://plugins.gradle.org/m2/")
        maven(url = "https://dl.bintray.com/kotlin/kotlin-js-wrappers")
    }
}

subprojects {

    apply {
        plugin("org.jetbrains.kotlin.js")
        plugin("org.jetbrains.kotlin.plugin.serialization")
        plugin("maven-publish")
    }

    kotlin {
        js {
            browser {
                testTask {
                    enabled = false
                }
            }
            compilations.all {
                kotlinOptions {
                    moduleKind = "commonjs"
                    outputFile = "$projectDir/build/classes/main/${project.name}.js"
                    metaInfo = true
                    sourceMap = true
                    sourceMapEmbedSources = "always"
                }
            }
        }
    }

    val kotlinSourcesJar by tasks
    publishing {
        repositories {
            mavenLocal()
            maven {
                name = "proteiCentral"
                url = uri("https://repo.protei.ru/repository/ext-snapshot-local")
                credentials {
                    username = findProperty("credentialsProteiCentralUsername") as String?
                    password = findProperty("credentialsProteiCentralPassword") as String?
                }
            }
        }
        publications {
            create<MavenPublication>("maven") {
                from(components["kotlin"])
                artifact(kotlinSourcesJar)
                groupId = project.group.toString()
                artifactId = project.name
                version = project.version.toString()
            }
        }
    }
}
