rootProject.buildFileName = "build.gradle.kts"
rootProject.name = "reactory"
include(
    "reactory-core",
    "reactory-components"
)
